import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="video-container">
      <video autoPlay="true" loop="true" muted src="https://www.lucidmotors.com/static/e4b786f3232a4ec91c3148153386bddf/homepage-hero-bg.mp4"></video>
      <nav className="customized-navbar navbar navbar-expand-lg flex-container">
        <div>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link text-white" href="#">LUCID AIR</a>
            </li>
            <li className="nav-item ml-2">
              <a className="nav-link text-white" href="#">DESIGN YOURS</a>
            </li>
            <li className="nav-item ml-2">
              <a className="nav-link text-white" href="#">RESERVE</a>
            </li>
          </ul>
        </div>
        <div>
          <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDQiIGhlaWdodD0iMTAiPgogIDxwYXRoIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTE0MC4xMDQgNC45OGMwLTEuNzktLjgwOC0yLjU1My0yLjcwMy0yLjU1M2gtMTcuMTk2VjcuNmgxNy4xOTZjMS44NyAwIDIuNzAzLS44NDUgMi43MDMtMi42MnptMy44OTYgMGMwIDIuNjA3LTIuMzI0IDQuMDY3LTQuNTEgNC4wNjdoLTIyLjk3Vi45OGgyMi45N2MyLjE4NiAwIDQuNTEgMS40MDIgNC41MSA0ek03NC43OTEgMi40MjdoMjAuODgxVi45OEg3Mi43MDNjLTIuMTg3IDAtNC41MSAxLjQwMi00LjUxIDQgMCAyLjYwOCAyLjMyMyA0LjA2OCA0LjUxIDQuMDY4aDIyLjk2OVY3LjZoLTIwLjg4Yy0xLjg3IDAtMi43MDQtLjg0NS0yLjcwNC0yLjYyIDAtMS43OS44MDgtMi41NTMgMi43MDMtMi41NTN6TTYuMTYxIDcuNTgxYy0xLjcxMyAwLTIuNDc2LS43MjUtMi40NzYtMi4zNTFWLjk1OEgwVjUuMjNjMCAyLjQ2NyAyLjIgMy44IDQuMjcxIDMuOGgyMi4zMzJWNy41OEg2LjE2ek01Ni45OC45OHY2LjYyNEgzOS41NjhjLTEuNzEyIDAtMi40NzUtLjcyNS0yLjQ3NS0yLjM1MlYuOThoLTMuNjg1djQuMjcyYzAgMi40NjggMi4yIDMuOCA0LjI3MSAzLjhoMjIuOTg3Vi45NzloLTMuNjg1em00Ny4zMjYgOC4yNTJoMy41NzhWLjc2OWgtMy41Nzh2OC40NjJ6Ii8+Cjwvc3ZnPgo=" alt="logo" />
        </div>
        <div>
          <button className="transparent-btn text-white">SIGN IN</button>
          <button className="ml-3 white-border-btn text-white">STAY UPDATED</button>
          <button className="ml-3 transparent-btn text-white"><i className="fa fa-bars"></i></button>
        </div>
      </nav>
      <div className="title-container pb-5">
          <div className="text-white">
            Lucid Air
          </div>
          <div>
            <button className="mr-3 discover-btn">Discover Air</button>
            <button className="transparent-btn text-white reserve-btn">Reserve Now</button>
          </div>
      </div>  
      <div className="position-absolute footer-container text-white d-flex justify-content-around">
        <div className="text-center">
          <div>Projected range²</div>
          <div className="new-font h1 mt-2">500+ mi</div>
        </div>
        <div className="text-center">
          <div>Starting from</div>
          <div className="new-font h1 mt-2">&#36; 69,900</div>
        </div>
        <div className="text-center">
          <div>Max power</div>
          <div className="new-font h1 mt-2">1080 hp</div>
        </div>
      </div>
    </div>
  );
}

export default App;
